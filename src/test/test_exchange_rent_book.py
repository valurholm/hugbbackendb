from app.services.exchange_service import ExchangeService
from app.services.rental_service import RentalService
from app.services.book_service import BookService
import unittest

""" A test suite that tests the backend's exchange/rental operations. """
class testExchangeBook(unittest.TestCase):
    """ Code that is run before every test case. """
    def setUp(self):
        self.__rental_service = RentalService()
        self.__exchange_service = ExchangeService()
        self.__book_service = BookService()
        self.test_rent1 = {
            "lender_id": 1,
            "book_id": 1,
            "renter_id": 2
        }
        self.test_rent2 = {
            "lender_id": 2,
            "book_id": 55,
            "renter_id": 1
        }
        self.test_exchange1 = {
            "userid1": 1,
            "userid2": 2,
            "bookid1": 1,
            "bookid2": 2
        }
        self.test_exchange2 = {
            "userid1": 23,
            "userid2": 12,
            "bookid1": 65,
            "bookid2": 33
        }
        self.__created_exchange = self.__exchange_service.create_exchange(self.test_exchange1)
        self.__created_rent = self.__rental_service.create_rental(self.test_rent1)
    
    """ Code that is run after every test case """
    def tearDown(self):
        self.__exchange_service.hard_delete_exchange(self.__created_exchange["id"])
        self.__exchange_service.decrement_next_exchange_id()
        self.__rental_service.hard_delete_rental(self.__created_rent["id"])
        self.__rental_service.decrement_next_rental_id()

    """ Tests whether the create_rental() method actually creates and stores the
        new rental. """
    def test_create_rental_creates_rental(self):
        dummy_rental = self.__created_rent
        self.assertEqual(self.__rental_service.get_next_rental_id()-1, dummy_rental["id"])
        self.assertEqual(self.test_rent1["lender_id"], dummy_rental["lender_id"])
        self.assertEqual(self.test_rent1["book_id"], dummy_rental["book_id"])
        self.assertEqual(self.test_rent1["renter_id"], dummy_rental["renter_id"])

    """ Tests if the create_exchange() method actually creates and stores the
        new exchange. """
    def test_create_exchange_creates_exchange(self):
        self.assertEqual(self.__exchange_service.get_next_exchange_id()-1, self.__created_exchange["id"])
        self.assertEqual(self.test_exchange1["userid1"], self.__created_exchange["userid1"])
        self.assertEqual(self.test_exchange1["userid2"], self.__created_exchange["userid2"])
        self.assertEqual(self.test_exchange1["bookid1"], self.__created_exchange["bookid1"])
        self.assertEqual(self.test_exchange1["bookid2"], self.__created_exchange["bookid2"])

    """ Test if delete_exchange works as intended. """
    def test_delete_exchange(self):
        exchange_id = self.__created_exchange["id"]
        self.__exchange_service.delete_exchange(exchange_id)
        self.assertEqual(self.__exchange_service.get_exchange(exchange_id)["status"], 3)

    """ Tests if delete_exchange does nothing if key is not found. """
    def test_delete_exchange_key_not_found(self):
        exchange_id = "test"
        with self.assertRaises(ValueError):
            self.__exchange_service.delete_exchange(exchange_id)

    """
        Tests whether the create_exchange() method throws a value error when
        given wrong parameter values.
    """
    def test_create_exchange_throws_value_error(self):
        with self.assertRaises(ValueError):
            self.__exchange_service.create_exchange(None)
    
    """
        Tests whether the make_book_unavailable() method updates a book to have
        a status of 'unavailable'
    """
    def test_make_book_unavailable_makes_book_unavailable(self):
        book_id = -1
        self.__book_service.remove_book(book_id)
        book_obj = self.__book_service.get_book_by_id(-1)
        self.assertEqual(3, book_obj["status"])
    
    """
        Tests whether the make_book_available() method updates a book to have a
        status of 'available'
    """
    def test_make_book_available_makes_book_available(self):
        book_id = -1
        self.__book_service.change_book_availability(book_id, 1)
        book_obj = self.__book_service.get_book_by_id(-1)
        self.assertEqual(1, book_obj["status"])

    """
        Tests whether the is_book_available() method returns True if a book is
        indeed available
    """
    def test_is_book_available_returns_true_when_available(self):
        book_id = -1
        self.__book_service.change_book_availability(book_id, 1)
        is_available = self.__book_service.is_book_available(book_id)
        self.assertTrue(is_available)

    """
        Tests whether the is_book_available() method returns False if a book is
        not available
    """
    def test_is_book_available_returns_false_when_unavailable(self):
        # Make book unavailable
        book_id = -1
        self.__book_service.remove_book(book_id)
        is_available = self.__book_service.is_book_available(book_id)
        self.assertFalse(is_available)

    """
        A helper function for getting the latest rental in the 'rentals'
        dictionary in dummydata.
    """
    def __get_latest_rental(self):
        rental_id = self.__rental_service.get_next_rental_id() - 1
        return self.__rental_service.get_rental(rental_id)

    """
        A helper function for getting the latest exchange in the 'exchanges'
        dictionary in dummydata.
    """
    def __get_latest_exchange(self):
        exchange_id = self.__exchange_service.get_next_exchange_id() - 1
        return self.__exchange_service.get_exchange(exchange_id)

if __name__ == "__main__":
    unittest.main()
