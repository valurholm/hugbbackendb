from app.services import user_service
import unittest

""" Tests the functionality of removing a certain user from the system """
class TestRemoveUser(unittest.TestCase):
    """ Code that is run before every test case """
    def setUp(self):
        self.__user_service = user_service.UserService()
        self.DummyUser = {"name": "John Johnsson","email": "jon@jonsson.com","username": "jonsson","password": "yomama", "is_admin": False}
        self.DummyAdmin = {"name": "John Johnsson","email": "admin@admin.com","username": "jonsson","password": "yomama", "is_admin": True}
        self.__added_user = self.__user_service.add_user(self.DummyUser)
        self.__added_admin = self.__user_service.add_user(self.DummyAdmin)

    """ Code that is run after every test case """
    def tearDown(self):
        self.__user_service.hard_delete_user(self.__added_user["id"])
        self.__user_service.hard_delete_user(self.__added_admin["id"])
        self.__user_service.decrement_next_user_id()
        self.__user_service.decrement_next_user_id()
 
    """ Tests if the remove_user() method doesn't allow the removal of admins """
    def test_user_isadmin(self):
        with self.assertRaises(ValueError):
            self.__user_service.remove_user(self.__added_admin["id"])

    """ Tests if the remove_user() method actually removes the specified user """
    def test_user_exists(self):
        self.__user_service.remove_user(self.__added_user["id"])
        removed_user = self.__user_service.get_user(self.__added_user["id"])
        self.assertEqual(removed_user["status"], 2)

    """
        Tests if the remove_user() method returns an error message if the
        specified ID does not exist.
    """
    def test_user_not_exists(self):
        fakeid = -10000
        with self.assertRaises(ValueError):
            self.__user_service.remove_user(fakeid)

    """
        Tests if the remove_user() method returns an error message if the ID is
        not of the right type
    """
    def test_unexpected_input(self):
        fakeid = "d"
        with self.assertRaises(ValueError):
            self.__user_service.remove_user(fakeid)
    

if __name__ == "__main__":
       unittest.main()