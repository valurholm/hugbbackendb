from app.services import user_service
import unittest

""" A class that tests the retrieval of a list of all users """
class TestGetUserList(unittest.TestCase):
    """ Code that is run before every test. """
    def setUp(self):
        self.__user_service = user_service.UserService()
        self.Users = self.__user_service.get_all_users()

    def tearDown(self):
        self.Users = ""

    """ Tests if the get_all_users() method returns all user objects """
    def test_view_all_users(self):
        all_users = list(self.__user_service.get_all_users().values())
        all_users_two = list(self.Users.values())
        self.assertEqual(all_users, all_users_two)

    """
        Tests if the get_all_users() method doesn't return just an empty string
    """
    def test_compare_wrong_results(self):
        self.assertNotEqual(self.__user_service.get_all_users(),"")


if __name__ == "__main__":
    unittest.main()