from app.repositories.book_repository import BookRepository
from app.repositories.user_repository import UserRepository
from app.repositories.session_repository import SessionRepository
from app.utility.session_id_maker import SessionIdMaker

"""
The service class for books. Checks the input the user gives and calls the Repository layer to
recieve data or change it.
"""
class BookService(object):
    """ Initializes the class with an instance of the class BookRepository """
    def __init__(self):
        self.__book_repository = BookRepository()
        self.__user_repository = UserRepository()
        self.__session_repository = SessionRepository()

    """ Checks if the name input is valid and returns the right book from the database """
    def get_book(self, name):
        if self.__check_if_book_exists(name) == False:
            return "Book doesn't exist!"
        elif self.__check_name_type(name):
            return "Invalid book name!"
        return self.__book_repository.get_book_by_name(name)
    
    """ Returns a book with the given bookid """
    def get_book_by_id(self, book_id):
        if not isinstance(book_id, int):
            raise ValueError("book ID must be an integer")
        return self.__book_repository.get_book(book_id)

    """ Returns all books in the database """
    def get_all_books(self):
        return self.__book_repository.get_all_books()
    
    """ Checks if the bookid input is valid and removes the book from the database """
    def remove_book(self, bookid):
        if isinstance(bookid, int) == False:
            return "Invalid Book ID"
        if bookid > self.__book_repository.get_next_book_id():
            return "Book ID out of bounds"
        else:
            self.__book_repository.remove_book(bookid)
    
    """ Actually deletes a book from the system (only used in test files) """
    def hard_delete_book(self, bookid):
        if isinstance(bookid, int) == False:
            return "Invalid Book ID"
        if bookid > self.__book_repository.get_next_book_id():
            return "Book ID out of bounds"
        else:
            self.__book_repository.hard_delete_book(bookid)

    """ Checks if the bookObj input is valid and adds the book to the database """
    def add_book(self, book_dict):
        self.__validate_book_dict(book_dict)
        return self.__book_repository.add_book(book_dict)

    """ Edits a book with the given ID. """
    def edit_book(self, book_obj, new_values):
        #I assume new_values is also a book_obj that has been editied with new values(ID cannot be edited)
        self.__validate_book_dict(book_obj)
        self.__validate_book_dict(new_values)

        # for i in range(2, len(book_obj)):
        #     book_obj.values()[i] = new_values.values()[i]

        if book_obj.get("user_id") != new_values.get("user_id"):
            return "Cannot change user ID of a book"

        for k,v in book_obj.items():
            if k == "" or v == None:
                return "Some required information was not given"
                
        book_obj.update(new_values)
        self.__book_repository.add_book(book_obj)
    
    """ Udates a book with the new book dictionary"""
    def update_book(self, book_id, session_id, updated_book_model):
        if not self.__book_repository.book_exists(book_id):
            raise ValueError("Book with ID={} does not exist".format(book_id))
        user_id = self.__session_repository.authenticate_user(session_id)
        if not user_id:
            raise ValueError("User must be signed in")
        user = self.__user_repository.get_user(user_id)
        book = self.__book_repository.get_book(book_id)
        if (user_id != book["user_id"]) and (not user["is_admin"]):
            return ValueError("User must be an admin or book owner to update book with ID={}".format(book_id))
        book["name"] = updated_book_model["name"]
        book["authors"] = updated_book_model["authors"]
        book["genres"] = updated_book_model["genres"]
        book["published"] = updated_book_model["published"]
        book["edition"] = updated_book_model["edition"]
        book["condition"] = updated_book_model["condition"]
        self.__book_repository.update_book(book)
        return book
    
    """ 
        Returns a dictionary of books that meet a certain search criteria,
        described in the search_model parameter.
    """
    def filter_books(self, search_model):
        all_books = self.__book_repository.get_all_books()
        valid_books = {}
        for book_id, book_model in all_books.items():
            if self.__meets_search_criteria(book_model, search_model):
                valid_books[book_id] = book_model
        return valid_books
    
    """ Updates a book with the given ID to have the status 'unavailable' """
    def change_book_availability(self, book_id, new_status):
        if not self.__book_repository.book_exists(book_id):
            raise ValueError(f"Cannot mark book that doesn't exist as unavailable (ID={book_id})")
        if new_status not in [1, 2]:
            raise ValueError(f"Can only change book availability to 1 or 2, not {new_status}")
        self.__book_repository.change_book_availability(book_id, new_status)
    
    """ Updates a book with the given ID to have the status 'available' """
    def make_book_available(self, book_id):
        book = self.__book_repository.get_book(book_id)
        book.set_status("available")
        self.__book_repository.add_book(book)
    
    """
        Checks if a book with the given ID is available or not, returns True if
        it is, otherwise False
    """
    def is_book_available(self, book_id):
        book = self.__book_repository.get_book(book_id)
        return book["status"] == 1
    
    """ Decrements the next available book ID (only for testing purposes) """
    def decrement_next_book_id(self):
        self.__book_repository.decrement_next_book_id()
    
    """ Checks if a certain book meets a certain search criteria """
    def __meets_search_criteria(self, book_model, search_model):
        is_valid = True
        if search_model["name"] and book_model["name"] != search_model["name"]:
            is_valid = False
        elif search_model["authors"] and not self.__equal_lists(book_model["authors"], search_model["authors"]):
            is_valid = False
        elif search_model["genres"] and not self.__equal_lists(book_model["genres"], search_model["genres"]):
            is_valid = False
        elif search_model["published"] and book_model["published"] != search_model["published"]:
            is_valid = False
        elif search_model["edition"] and book_model["edition"] != search_model["edition"]:
            is_valid = False
        elif search_model["condition"] and book_model["condition"] != search_model["condition"]:
            is_valid = False
        return is_valid
    
    """ Helper function to compare two lists"""
    def __equal_lists(self, list1, list2):
        if len(list1) != len(list2):
            return False
        same = True
        for index, value in enumerate(list1):
            if value != list2[index]:
                same = False
                break
        return same
    
    """ Checks if a book dictionary is valid """
    def __validate_book_dict(self, book_dict):
        if book_dict == None:
            raise ValueError("Some required information was not given")
        has_valid_properties, message = self.__check_book_dict_properties(book_dict)
        if not has_valid_properties:
            raise ValueError(message)
        has_valid_types, message = self.__check_book_dict_value_types(book_dict)
        if not has_valid_types:
            raise ValueError(message)
        if book_dict["name"] == "":
            raise ValueError("Model is missing property: {}".format(book_dict["name"]))
    
    """ Checks if a book dictionary has only the valid properties """
    def __check_book_dict_properties(self, book_dict):
        properties = ["user_id", "name", "authors", "genres", "published", "edition", "condition"]
        for prop in properties:
            if prop not in book_dict:
                return (False, "Model is missing property: {}".format(prop))
        if len(book_dict.keys()) != len(properties):
            return (False, "Model includes some unwanted properties")
        return (True, "All is well")
    
    """ Checks if a book dictionary has all the valid value types """
    def __check_book_dict_value_types(self, book_dict):
        if  type(book_dict["user_id"]) != int or \
            type(book_dict["name"]) != str or \
            type(book_dict["authors"]) != list or \
            type(book_dict["genres"]) != list or \
            type(book_dict["published"]) != int or \
            type(book_dict["edition"]) != int or \
            type(book_dict["condition"]) != int:
            return (False, "Some property/properties are not of the correct type")
        return (True, "All is well")

    """ Checks if a book already exists in the database that has the same name as the name input """
    def __check_if_book_exists(self, name):
        book_dict = self.__book_repository.get_all_books()
        for book in book_dict.values():
            if book["name"] == name:
                return True
        return False
    
    """ Checks if the name input is a string or not """
    def __check_name_type(self, name):
        if type(name) != str:
            return True
        return False
    
    """ Checks if all values in a list are of a certain type """
    def __is_list_of(self, lis, some_type):
        is_valid = True
        for elem in lis:
            if type(elem) != some_type:
                is_valid = False
                break
        return is_valid