import uuid

"""
  A class that encapsulates the functionality behind the creation of session
  ID's
"""
class SessionIdMaker(object):
    """ Generates and returns a new unique session ID """
    def create_session_id(self):
      return uuid.uuid4().hex[:30]