# Models

This system only uses Python dictionaries to encapsulate information instead of traditional classes since JSON is used to store and retrieve data. Here you can see how valid dictionaries of each type should look like in JSON format.

## Table of Contents
1. [Book](#book)
2. [Exchange](#exchange)
3. [Message](#message)
4. [Rental](#rental)
5. [User](#user)

---

## Book

### Example model
``` json
{
    "id": 3,
    "user_id": 2,
    "name": "Harry Potter and the Chamber of Secrets",
    "authors": ["J.K. Rowling"],
    "genres": ["Fantasy"],
    "published": 1999,
    "edition": 3,
    "condition": 1,
    "status": 1
}
```

### Property descriptions

| Property | Type | Description
|----------|------|------------
| id | Number | The unique ID of the book.
| user_id | Number | The user ID of the owner of the book.
| name | String | The title of the book.
| authors | List | The authors of the book.
| genres | List | The genres of the book (fantasy, sci-fi...)
| published | Number | The year that the book was published
| edition | Number | The edition of the book.
| condition | Number | The condition of the book, **1: great**, **2: mediocre**, **3: bad** condition.
| status | Number | The status of the book, **1: available**, **2: unavailable**, **3: removed**.

<br />

## Exchange

### Example model
``` json
{
    "id": 4,
    "userid1": 2,
    "userid2": 3,
    "bookid1": 1,
    "bookid2": 2,
    "status": 1
}
```

### Property descriptions

| Property | Type | Description
|----------|------|------------
| id | Number | The unique ID of the exchange.
| userid1 | Number | The ID of one participant of the exchange.
| userid2 | Number | The ID of the other participant of the exchange.
| bookid1 | Number | ID of *userid1*'s book.
| bookid2 | Number | ID of *userid2*'s book.
| status | Number | The status of the exchange **1: ongoing**, **2: finished**, **3: deleted**

<br />

## Message

### Example model
``` json
{
    "id": 1,
    "sender_id": 4,
    "receiver_id": 2,
    "timestamp": "09/28/2019 13:00:00",
    "message": "That relatable moment when :|"
}
```

### Property descriptions

| Property | Type | Description
|----------|------|------------
| id | Number | The message's unique ID.
| sender_id | Number | ID of the sender of the message.
| receiver_id | Number | ID of the receiver of the message.
| timestamp | String | The date and time when the message was sent, on the format: MM/dd/yyyy hh:mm:ss.
| message | String | The contents of the message.

<br />

## Rental

### Example model
``` json
{
    "id": 4,
    "lender_id": 2,
    "book_id": 3,
    "renter_id": 1,
    "status": 1
}
```

### Property descriptions

| Property | Type | Description
|----------|------|------------
| id | Number | The unique ID for the rental
| lender_id | Number | The user ID of the lender (the person giving away his book temporarily)
| book_id | Number | The ID of *lender_id*'s book.
| renter_id | Number | The ID of the renter (the person accepting *lender_id*'s book).
| status | Number | The status of the rental, **1: ongoing**, **2: closed**, **3: removed**.

<br />

## User

### Example model
``` json
{
    "id": 6,
    "name": "Barry B. Benson",
    "email": "barry@bmail.com",
    "username": "be-more-bee",
    "password": "you-like-jazz",
    "is_admin": false,
    "status": 1
}
```

### Property descriptions

| Property | Type | Description
|----------|------|------------
| id | Number | The unique ID for the user
| name | String | The user's full name
| email | String | A user's email (**must be valid**)
| username | String | The user's username
| password | String | The user's password
| is_admin | Boolean | `True` if the user is an admin, otherwise `False`.
| status | Number | The status of the user **1: available**, **2: deleted**